package models

import akka.actor.{Actor, ActorRef, Props}
import play.api.libs.json.{JsValue, Json}

case class Player(name: String, isAdmin: Boolean)

case class ConnectedPlayer(private val player: Player, private val out: ActorRef) {
  val id: Int = ConnectedPlayer.nextId()
  val name: String = player.name
  val isAdmin: Boolean = player.isAdmin
  var onPlayerEvent: (PlayerEvents.Value, JsValue) => Unit = _

  val props = Props(new Actor {
    override def receive: Receive = {
      case msg: JsValue => dispatchMessage(msg)
    }

    override def postStop(): Unit = {
      super.postStop()
      onPlayerEvent(PlayerEvents.disconnect, null)
    }
  })

  def sendMessage(message: String, data: JsValue): Unit = out ! Json.obj(
    "message" -> message,
    "data" -> data,
  )

  private def dispatchMessage(msg: JsValue): Unit = {
    (msg \ "message").validate[String] map {
      case "run" if player.isAdmin => onPlayerEvent(PlayerEvents.startGame, null)
      case "reset" if player.isAdmin => onPlayerEvent(PlayerEvents.resetGame, null)
      case "playerTry" => onPlayerEvent(PlayerEvents.playerResult, (msg \ "data").get)
      case "heartBeat" => sendMessage("heartBeat", Json.obj())
      case _ => println(s"Unhandled message $msg")
    }
  }
}

private object FromPlayerMessages extends Enumeration {
  val run: Value = Value("run")
}

object PlayerEvents extends Enumeration {
  val disconnect: Value = Value("disconnect")
  val startGame: Value = Value("startGame")
  val resetGame: Value = Value("resetGame")
  val playerResult: Value = Value("playerResult")
}

object ConnectedPlayer {
  private var id = 0

  def nextId(): Int = {
    id += 1
    id
  }
}

