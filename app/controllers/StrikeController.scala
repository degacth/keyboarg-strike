package controllers

import akka.actor.ActorSystem
import akka.stream.Materializer
import javax.inject._
import models.Game
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.streams.ActorFlow

@Singleton
class StrikeController @Inject()(components: ControllerComponents)
                                (implicit system: ActorSystem, mat: Materializer)
  extends AbstractController(components) {

  def index(): Action[AnyContent] = Action { implicit request =>
    Ok(views.html.strike())
  }

  def ws(): WebSocket = WebSocket.accept[JsValue, JsValue] { implicit request =>
    ActorFlow.actorRef {
      out => Game playerConnected out
    }
  }
}
