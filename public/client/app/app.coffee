marshal = JSON.stringify.bind JSON
unMarshal = JSON.parse.bind JSON

ws = new WebSocket appConf.socketUrl

sendMessage = (message, data) -> ws.send marshal {message, data}

gameStates =
    onStart: "ready"
    isRunning: "running"
    isFinished: "finished"

messages =
    run: "run"
    reset: "reset"
    playerTry: "playerTry"
    heartBeat: "heartBeat"

app = new Vue
    el: "#app"

    data:
        state: ""
        playerName: ""
        players: []
        isAdmin: no
        step: {}
        usersResult: []
        finishResults: []

    methods:
        gameOnStart: -> @state is gameStates.onStart
        gameIsRunning: -> @state is gameStates.isRunning
        gameIsFinished: -> @state is gameStates.isFinished
        onRun: -> sendMessage messages.run, null
        resetGame: -> sendMessage messages.reset, null
        onSuccessTry: (pos, subtask) -> sendMessage messages.playerTry, {pos, result: subtask}
        getResetButtonCssClass: -> {invisible: @state is gameStates.isRunning}

messageCallbacks =
    playerList: ({players}) ->
        app.players = players

    gameStateChanged: ({state}) ->
        app.state = state

    connected: ({name, state, isAdmin}) ->
        app.playerName = name
        app.state = state
        app.isAdmin = isAdmin

    nextStep: (step) ->
        app.step = step
        app.usersResult = []

    heartBeat: ->
    stepResults: ({results}) -> app.usersResult = results
    finishResults: ({results}) ->
        app.finishResults = results
            .sort ([__, r1], [_1, r2]) -> r2 - r1
            .map ([name, wins]) -> {name, wins}

ws.onmessage = ({data}) ->
    unless data then throw new Error "empty data"
    obj = unMarshal data
    messageCallbacks[obj.message] obj.data

ws.onopen = ->
    while yes
        await window.sleep 15000
        sendMessage messages.heartBeat, Date.now()
